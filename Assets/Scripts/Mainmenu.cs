﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Mainmenu : MonoBehaviour
{
    public void btn_loadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
