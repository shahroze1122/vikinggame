﻿using System;
using UnityEngine;

public class PhysicsBasedSingleAxisRotationTypeController : MonoBehaviour, IUtencil
{
    public float horizontalInput { get; set; }
    public float verticalInput { get; set; }
    public bool isDragging { get; set; }
    public Action onTapAction { get; set; }

    [SerializeField] private RotationAxis axisToControl = RotationAxis.Z;
    [SerializeField] private float rotationSpeed = .1f;
    [SerializeField] private float minRotation = -30f;
    [SerializeField] private float maxRotation = 30f;
    [SerializeField] private float rotationSmoothing = .8f;

    Rigidbody rb;
    private float angle = 0f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if(!isDragging)
            return;

        handleInput();
    }

    public void handleInput()
    {
        if(horizontalInput != 0)
            angle += horizontalInput * rotationSpeed * -1f;

        angle = Mathf.Clamp(angle, minRotation, maxRotation);
        Vector3 targetVector = Vector3.zero;
        switch(axisToControl)
        {
            case RotationAxis.X:
                targetVector = new Vector3(angle, 0f, 0f);
                break;
            case RotationAxis.Y:
                targetVector = new Vector3(0f, angle, 0f);
                break;
            case RotationAxis.Z:
                targetVector = new Vector3(0f, 0f, angle);
                break;
            default:
                targetVector = Vector3.zero;
                break;
        }
        rb.MoveRotation(Quaternion.Slerp(transform.rotation, Quaternion.Euler(targetVector), rotationSmoothing));
    }
}

public enum RotationAxis
{
    X, Y, Z
}