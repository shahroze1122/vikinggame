﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public IUtencil currentUtencil;
    public PlayerInput playerInput;

    private void Awake()
    {
        Application.targetFrameRate = 60;

        var utencils = FindObjectsOfType<MonoBehaviour>().OfType<IUtencil>().ToList();

        if(utencils != null)
        {
            if(utencils.Count != 0)
            {
                currentUtencil = utencils[0];
            }
        }
        else
        {
            Debug.Log("! ! ! W A R N I N G ! ! !  No utencil in scene.");
        }
    }

    public void setCurrentUtencil(IUtencil utencil)
    {
        currentUtencil = utencil;
        playerInput.uc = utencil;
    }
}
