﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerInput : MonoBehaviour, /*IDragHandler, IBeginDragHandler, IEndDragHandler,*/ IPointerDownHandler
{
    [SerializeField] private float inputDeltaXUpperLimit = 10f;
    [SerializeField] private float inputDeltaYUpperLimit = 10f;

    public IUtencil uc { get; set; }

    private void Start()
    {
        GameManager gm = FindObjectOfType<GameManager>();
        uc = gm.currentUtencil as IUtencil;
    }

    //public void OnBeginDrag(PointerEventData eventData)
    //{
    //    Debug.Log("Begin Drag...");
    //    uc.isDragging = true;
    //}

    //public void OnDrag(PointerEventData eventData)
    //{
    //    Debug.Log("Dragging...");
    //    uc.horizontalInput = eventData.delta.x;
    //    uc.verticalInput = eventData.delta.y;
    //}

    //public void OnEndDrag(PointerEventData eventData)
    //{
    //    Debug.Log("End Drag...");
    //    uc.isDragging = false;
    //}

    public void OnPointerDown(PointerEventData eventData)
    {
        if(uc == null)
            return;

        uc.onTapAction?.Invoke();
    }

    private void Update()
    {
        if(uc == null)
            return;

        if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if(touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
            {
                uc.isDragging = true;
                Debug.Log($"touchDelta={touch.deltaPosition}");
                uc.horizontalInput = touch.deltaPosition.x * .1f;
                uc.verticalInput = touch.deltaPosition.y * .1f;
                uc.horizontalInput = Mathf.Clamp(uc.horizontalInput, -inputDeltaXUpperLimit, inputDeltaXUpperLimit);
                uc.verticalInput = Mathf.Clamp(uc.verticalInput, -inputDeltaYUpperLimit, inputDeltaYUpperLimit);
            }
            else
            {
                uc.isDragging = false;
            }
        }
    }
}
