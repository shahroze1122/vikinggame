﻿using System;
using System.Linq;
using UnityEngine;

public class RotationalController : MonoBehaviour, IUtencil
{
    public float horizontalInput { get; set; }
    public float verticalInput { get; set; }
    public bool isDragging { get; set; }
    public Action onTapAction { get; set; }

    [Header("Horizontal Controls")]
    [SerializeField]private float rotationSpeedZ = .1f;
    [SerializeField]private float minZRotation = -30f;
    [SerializeField]private float maxZRotation = 30f;

    [Header("Veritcal Controls")]
    [SerializeField]private float rotationSpeedX = .1f;
    [SerializeField]private float minXRotation = -30f;
    [SerializeField]private float maxXRotation = 30f;

    [Header("Common Parameters")]
    [SerializeField]private float rotationSmoothing = .8f;

    private Rigidbody rb;
    private float angleZ = 0f;
    private float angleX = 0f;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        var gm = FindObjectOfType<GameManager>();
        gm.setCurrentUtencil(this);

        var fryingItems = GetComponentsInChildren<FriedItemScript>();
        if(fryingItems != null)
        {
            if(fryingItems.Length != 0)
            {
                for(int i = 0; i < fryingItems.Length; i++)
                {
                    fryingItems[i].transform.SetParent(null, true);
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if(!isDragging)
            return;

        handleInput();
    }

    public void handleInput()
    {
        if(horizontalInput != 0)
            angleZ += horizontalInput * rotationSpeedZ * -1f;
        if(verticalInput != 0)
            angleX += verticalInput * rotationSpeedX * 1f;

        angleZ = Mathf.Clamp(angleZ, minZRotation, maxZRotation);
        angleX = Mathf.Clamp(angleX, minXRotation, maxXRotation);
        var targetVector = new Vector3(angleX, 0f, angleZ);
        rb.MoveRotation(Quaternion.Slerp(transform.rotation, Quaternion.Euler(targetVector), rotationSmoothing));
        //rb.MoveRotation(Quaternion.Euler(targetVector));
    }
}
