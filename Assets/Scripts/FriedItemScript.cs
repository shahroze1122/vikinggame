﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FriedItemScript : MonoBehaviour
{
    [SerializeField] private float maxDetectionDistance;
    [SerializeField] private float fillUpSpeed = .5f;

    [Header("Spring Settings")]
    [SerializeField] private float minForce = 8f;
    [SerializeField] private float maxForce = 100f;
    [SerializeField] private float forceDistanceFactor = .8f;
    [SerializeField] private float maxVelocity = 1.0f;

    [Header("Progress View Parameters")]
    [SerializeField] private Transform fryingProgressCanvas;
    [SerializeField] private Image fryingProgressImage;
    [SerializeField] private Vector3 progressPos = Vector3.zero;
    private bool isDoneCooking = false;
    private FryingDetectionPoint[] detectionPoints;
    private float totalProgress = 0.0f;

    private int utencilLayerIndex;
    private Material[] itemMats;

    private Rigidbody rb;
    private SpringJoint springJointComponent;

    private void Awake()
    {
        detectionPoints = GetComponentsInChildren<FryingDetectionPoint>();

        utencilLayerIndex = 1 << LayerMask.NameToLayer("Utencil");
        isDoneCooking = false;

        itemMats = GetComponent<Renderer>().materials;
        rb = GetComponent<Rigidbody>();
        springJointComponent = GetComponent<SpringJoint>();

        itemMats.Reverse();
    }

    private void Start()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, Vector3.down, out hit, Mathf.Infinity, utencilLayerIndex))
        {
            var urb = hit.collider.GetComponent<Rigidbody>();
            if(urb != null)
            {
                springJointComponent.connectedBody = urb;
            }
        }

        fryingProgressCanvas.SetParent(null, true);
    }

    private void Update()
    {
        fryingProgressCanvas.position = transform.position + progressPos;

        if(isDoneCooking)
            return;

        if(totalProgress >= 1.0f)
        {
            isDoneCooking = true;
            GetComponentInParent<RecipeScript>()?.onRecipeStepCompleted();
        }
    }

    private void FixedUpdate()
    {
        if(isDoneCooking)
            return;

        #region Spring Setting
        //float springForce = minForce + Vector3.Distance(springJointComponent.connectedAnchor, transform.position) * forceDistanceFactor;
        //springForce = Mathf.Clamp(springForce, minForce, maxForce);
        //springJointComponent.spring = springForce;
        #endregion

        #region set spring anchor based on raycast hit
        RaycastHit springConnectionPointHitInfo;
        if(Physics.Raycast(transform.position, Vector3.down, out springConnectionPointHitInfo, Mathf.Infinity, utencilLayerIndex))
        {
            //Debug.DrawRay(transform.position, Vector3.down, Color.cyan);
            springJointComponent.connectedAnchor = springConnectionPointHitInfo.transform.InverseTransformPoint(springConnectionPointHitInfo.point);
        }
        #endregion

        #region raycast hit detection for cooking prpogress
        for(int i = 0; i < detectionPoints.Length; i++)
        {
            Debug.DrawRay(detectionPoints[i].transform.position, detectionPoints[i].transform.up, Color.red);

            RaycastHit detectionPointHitInfo;
            if(Physics.Raycast(detectionPoints[i].transform.position, detectionPoints[i].transform.up, out detectionPointHitInfo, maxDetectionDistance, utencilLayerIndex))
            {
                if(detectionPointHitInfo.collider.GetComponent<IUtencil>() != null)
                {
                    if(detectionPoints[i].fryingProgress < (1.0f / (float)detectionPoints.Length))
                    {
                        // show progress meter for this side and fill up
                        detectionPoints[i].fryingProgress += Time.deltaTime * fillUpSpeed;

                        fryingProgressImage.transform.parent.gameObject.SetActive(true);
                        fryingProgressImage.fillAmount = detectionPoints[i].fryingProgress / (1.0f / (float)detectionPoints.Length);
                        //Debug.Log($"hit detected: {detectionPoints[i].name}:{detectionPointsProgressDict[detectionPoints[i]]}");
                        break;
                    }
                    else
                    {
                        fryingProgressImage.transform.parent.gameObject.SetActive(false);
                    }
                }
            }
            else
            {
                fryingProgressImage.transform.parent.gameObject.SetActive(false);
            }
        }
        #endregion

        //if(rb.velocity.magnitude > maxVelocity)
        //    rb.velocity = rb.velocity.normalized * maxVelocity;

        totalProgress = 0.0f;
        for(int i = 0; i < detectionPoints.Length; i++)
        {
            totalProgress += detectionPoints[i].fryingProgress;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.layer == utencilLayerIndex)
        {
            Debug.Log("! ! ! Out Of Pan ! ! !");
            //EditorApplication.isPaused = true;
        }
    }
}
