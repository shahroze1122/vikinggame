﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularSpawner : MonoBehaviour
{
    public bool useExistingChildren;
    public bool getValuesFromEditor;

    [Header("Arrangement Parameters")]
    public int gameobjectCount = 20;
    public PhysicMaterial colliderMaterial;
    public float radius = .5f;
    public float arrangementOffset = 0.0f;

    [Header("Collider Parameters")]
    public Vector3 collidersRootPosition = Vector3.zero;
    public Vector3 scaleSetting = Vector3.one;
    public float rotationOffset = 1.5f;

    [Header("Top Collider Parameters")]
    public Vector3 topColliderScale = Vector3.one;
    public Vector3 topColliderPosition = Vector3.zero;

    [Space]
    public Transform topCollider;
    public List<Transform> transformsToArrange;

    private void Start()
    {
        if(useExistingChildren)
        {
            if(transformsToArrange != null)
            {
                transformsToArrange.Clear();
            }

            for(int i = 0; i < transform.childCount; i++)
            {
                if(!transform.GetChild(i).name.Contains("TOP"))
                    transformsToArrange.Add(transform.GetChild(i));
                else
                    topCollider = transform.GetChild(i);
            }

            if(getValuesFromEditor)
            {
                collidersRootPosition = transform.localPosition;
                topColliderScale = topCollider.localScale;
                topColliderPosition = topCollider.localPosition;
            }
        }
        else
        {
            // spawn roof collider
            GameObject go = new GameObject("Collider - TOP");
            go.AddComponent<BoxCollider>().material = colliderMaterial;
            go.transform.parent = transform;
            go.transform.localPosition = Vector3.zero;
            go.transform.localRotation = Quaternion.identity;
            go.transform.localScale = Vector3.one;
            topCollider = go.transform;

            // surrounding colliders
            transformsToArrange = new List<Transform>();
            for(int i = 0; i < gameobjectCount; i++)
            {
                GameObject colliderParentGO = new GameObject($"colliderParent ({i+1})");
                colliderParentGO.transform.parent = transform;
                colliderParentGO.transform.localPosition = Vector3.zero;
                colliderParentGO.transform.localRotation = Quaternion.identity;
                colliderParentGO.transform.localScale = Vector3.one;
                GameObject colliderGO = new GameObject($"collider ({i+1})");
                colliderGO.transform.parent = colliderParentGO.transform;
                colliderGO.transform.localPosition = Vector3.zero + (Vector3.up * 0.5f);
                colliderGO.transform.localRotation = Quaternion.identity;
                colliderGO.transform.localScale = Vector3.one;
                colliderGO.AddComponent<BoxCollider>().material = colliderMaterial;
                transformsToArrange.Add(colliderParentGO.transform);
            }
        }
    }

    private void Update()
    {
        topCollider.localScale = topColliderScale;
        topCollider.localPosition = topColliderPosition;
        transform.localPosition = collidersRootPosition;

        for(int i = 0; i < transformsToArrange.Count; i++)
        {
            var angle = (i+arrangementOffset) * Mathf.PI * 2 / transformsToArrange.Count;
            var pos = new Vector3(Mathf.Cos(angle), 0f, Mathf.Sin(angle)) * radius;

            transformsToArrange[i].localPosition = pos;
            transformsToArrange[i].LookAt(transform.position);
            transformsToArrange[i].localRotation *= Quaternion.Euler(rotationOffset, 0f, 0f);
            transformsToArrange[i].localScale = scaleSetting;
        }
    }
}
