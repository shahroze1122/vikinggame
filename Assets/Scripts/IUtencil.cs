﻿using System;

public interface IUtencil
{
    float horizontalInput { get; set; }
    float verticalInput { get; set; }
    Action onTapAction { get; set; }
    bool isDragging { get; set; }
    void handleInput();
}
