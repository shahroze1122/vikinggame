﻿using UnityEngine;

[CreateAssetMenu(fileName = "RecipeManagerSO", menuName = "Scriptable Object/Recipe Manager SO", order = 1)]
public class RecipeManagerSO : ScriptableObject
{
    public GameObject[] recipes;
}
